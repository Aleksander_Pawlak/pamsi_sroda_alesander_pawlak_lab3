// L3_1.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cstdlib>
#include <string>
#include "Stos.h"
#include "Kolejka1.h"
#include <ctime>
#include <Windows.h>
#include <stack>
#include <queue>
using namespace std;

/*template<typename Typ>
void hanoi(int I, Stos<Typ>& A, Stos<Typ>& B, Stos<Typ>& C, int krok_pause) {
	static int krok;
	krok++;
	cout << "A::\n"; A.display(); cout << "B::\n"; B.display(); cout << "C::\n"; C.display();
	if (krok == krok_pause) { cout << "STOP, KROL PARKIETU:::::   "; cout << "A::\n"; A.display(); cout << "B::\n"; B.display(); cout << "C::\n"; C.display();
	}
	else {
		if (I > 0) {  hanoi(I - 1, A, C, B, krok_pause); C.push(A.pop()); hanoi(I - 1, B, A, C, krok_pause);  }
	}
}*/


template<typename Typ>
void hanoi(int I, Stos<Typ>& A, Stos<Typ>& B, Stos<Typ>& C, int krok_pause) {
	static int krok(1);
	if (krok != krok_pause)
	try {
		krok++;
		if (krok == krok_pause) {
			cout << "STOP::::::: ILOSC KROKOW:: "<<krok<<"   "<< "A::\n"; A.display(); cout << "B::\n"; B.display(); cout << "C::\n"; C.display();
			//return;
			throw 1;
		}
		else {
			if (I > 0) { hanoi(I - 1, A, C, B, krok_pause); C.push(A.pop()); hanoi(I - 1, B, A, C, krok_pause); }
		}
	}
	catch (int e) { return; }
}


template<typename Typ>
void hanoi2(int I, Stos<Typ>& A, Stos<Typ>& B, Stos<Typ>& C, int krok_pause) {
	static int krok2(1);
	if (krok2 != krok_pause) {
		krok2++;
		if(krok2==krok_pause){
			cout << "STOP::::::: ILOSC KROKOW:: " << krok2 << "   " << "A::\n"; A.display(); cout << "B::\n"; B.display(); cout << "C::\n"; C.display();
			return;
		}
		else {
			if (I > 0) { hanoi(I - 1, A, C, B, krok_pause); C.push(A.pop()); hanoi(I - 1, B, A, C, krok_pause); }
		}
	}
}

void menu();
void menu2();
int main()
{
	srand(time(NULL));
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;
	QueryPerformanceFrequency(&frequency);
	int w_danych;// = 500000;
	/*Stos<int> C(3);
	int ef;
	C.push(1); C.push(2); C.push(3);///HANOI
	Stos<int> D(3);	
	Stos<int> E(3);
	cout<<"Podaj krok\n";
	cin>>ef;
	hanoi<int>(3, C, D, E,ef); cout << "T\n";*/
	cout << "podaj wielkosc danych\n"; cin >> w_danych;
	Stos<int> A(1);
	QueryPerformanceCounter(&t1);//start zegara
	for (int i(0); i < w_danych; i++) {//wypelnianie stosu losowymi liczbami
		A.push(rand());
	}
	//A.Delete();

	QueryPerformanceCounter(&t2);//stop zegara
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << elapsedTime << " ms.\n";
	
	
	stack<int> B;

	LARGE_INTEGER frequency2;        // ticks per second
	LARGE_INTEGER t3, t4;           // ticks
	double elapsedTime2;
	QueryPerformanceFrequency(&frequency2);
	QueryPerformanceCounter(&t3);//start zegara2

	for (int i(0); i < w_danych; i++) {//wypelnianie stosu losowymi liczbami
		B.push(rand());
	}
	QueryPerformanceCounter(&t4);
	elapsedTime2 = (t4.QuadPart - t3.QuadPart) * 1000.0 / frequency2.QuadPart;
	cout <<"Dla stl:  "<< elapsedTime2 << " ms.\n";
	////////HANOIIIIIIII

	Stos<int> C(3);
	int ef;
	for (int i(1); i < 21; i++) { C.push(i); }
	//C.push(1); C.push(2); C.push(3);///HANOI
	Stos<int> D(20);
	Stos<int> E(20);
	cout<<"Podaj krok\n";
	cin>>ef;
	hanoi2<int>(20, C, D, E,ef); cout << "T\n";

	////HANOIII
	system("PAUSE");
	return 0;
}



void menu() {
	int pom, pom2, pom3;// , n;
	do {
		cout << "Prosze wybra� opcje:\n";
		cout << "1.Stworz nowy stos\n";
		cout << "0. Zakoncz dzialanie programu\n";
		cin >> pom;
		switch (pom) {
		case 1: {
			cout << "\n\n Wybierz typ zmiennych:\n 1. int\n 2. float\n 3. string\n";
			cin >> pom2;
			switch (pom2) {
			case 1: {Stos<int> uzytkownik(6);
				do {
					cout << "Wybierz dzialania na stosie:\n";
					cout << "1. Dodaj nowy element na poczatek Stosu\n";
					cout << "2. Wyswietl zawartosc Stosu\n";
					cout << "3. Usun zawaartosc calego stosu\n";
					cout << "4. Usun pojedynczy element stosu\n";
					cout << "0. zakoncz dzialania na tym stosie\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {int we;  cout << "podaj wartosc elementu\n"; cin >> we; uzytkownik.push(we); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: {cout<<"usuwam:  "<<uzytkownik.pop()<<endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }//typ int
			case 2: {Stos<float> uzytkownik(5);
				do {
					cout << "Wybierz dzialania na stosie:\n";
					cout << "1. Dodaj nowy element na poczatek Stosu\n";
					cout << "2. Wyswietl zawartosc Stosu\n";
					cout << "3. Usun zawaartosc calego stosu\n";
					cout << "4. Usun pojedynczy element stosu\n";
					cout << "0. zakoncz dzialania na tym stosie\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {float we2; cout << "podaj wartosc elementu\n"; cin >> we2; uzytkownik.push(we2); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: { cout << "usuwam:  " << uzytkownik.pop()<<endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }//typ listy float
			case 3: {Stos<string> uzytkownik(5);
				do {
					cout << "Wybierz dzialania na stosie:\n";
					cout << "1. Dodaj nowy element na poczatek Stosu\n";
					cout << "2. Wyswietl zawartosc Stosu\n";
					cout << "3. Usun zawaartosc calego stosu\n";
					cout << "4. Usun pojedynczy element stosu\n";
					cout << "0. zakoncz dzialania na tym stosie\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {string we3; cout << "podaj wartosc elementu\n"; cin >> we3; uzytkownik.push(we3); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: { cout << "usuwam:  " << uzytkownik.pop()<<endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }// typ string*/
			}
			break;
		}//opcja 1-tworz liste
		case 0: break;
		default: cout << "Blednie wybrano opcje\n";
		}


	} while (pom != 0);
}



void menu2() {
	int pom, pom2, pom3;// , n;
	do {
		cout << "Prosze wybra� opcje:\n";
		cout << "1.Stworz nowa kolejke\n";
		cout << "0. Zakoncz dzialanie programu\n";
		cin >> pom;
		switch (pom) {
		case 1: {
			cout << "\n\n Wybierz typ zmiennych:\n 1. int\n 2. float\n 3. string\n";
			cin >> pom2;
			switch (pom2) {
			case 1: {Kolejka1<int> uzytkownik(6);
				do {
					cout << "Wybierz dzialania na kolejce:\n";
					cout << "1. Dodaj nowy element na poczatek kolejki\n";
					cout << "2. Wyswietl zawartosc kolejki\n";
					cout << "3. Usun zawaartosc calego kolejki\n";
					cout << "4. Usun pojedynczy element kolejki\n";
					cout << "0. zakoncz dzialania na tym kolejce\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {int we;  cout << "podaj wartosc elementu\n"; cin >> we; uzytkownik.push(we); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: {cout << "usuwam:  " << uzytkownik.pop() << endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }//typ int
			case 2: {Kolejka1<float> uzytkownik(5);
				do {
					cout << "Wybierz dzialania na kolejce:\n";
					cout << "1. Dodaj nowy element na poczatek kolejki\n";
					cout << "2. Wyswietl zawartosc kolejki\n";
					cout << "3. Usun zawaartosc calego kolejki\n";
					cout << "4. Usun pojedynczy element kolejki\n";
					cout << "0. zakoncz dzialania na tym kolejce\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {float we2; cout << "podaj wartosc elementu\n"; cin >> we2; uzytkownik.push(we2); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: { cout << "usuwam:  " << uzytkownik.pop() << endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }//typ listy float
			case 3: {Kolejka1<string> uzytkownik(5);
				do {
					cout << "Wybierz dzialania na kolejce:\n";
					cout << "1. Dodaj nowy element na poczatek kolejki\n";
					cout << "2. Wyswietl zawartosc kolejki\n";
					cout << "3. Usun zawaartosc calego kolejki\n";
					cout << "4. Usun pojedynczy element kolejki\n";
					cout << "0. zakoncz dzialania na tym kolejce\n";
					cin >> pom3;
					switch (pom3) {
					case 0:break;
					case 1: {string we3; cout << "podaj wartosc elementu\n"; cin >> we3; uzytkownik.push(we3); break; }
					case 2: { uzytkownik.display(); break; }
					case 3: { uzytkownik.Delete(); break; }
					case 4: { cout << "usuwam:  " << uzytkownik.pop() << endl; break; }
					default: cout << "Blednie wybrano opcje\n";
					}
				} while (pom3 != 0);
				break; }// typ string*/
			}
			break;
		}//opcja 1-tworz liste
		case 0: break;
		default: cout << "Blednie wybrano opcje\n";
		}


	} while (pom != 0);
}