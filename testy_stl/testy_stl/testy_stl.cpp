// testy_stl.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stack>
#include <queue>
#include <Windows.h>
#include<iostream>
#include<ctime>
#include <cstdlib>
using namespace std;
int main()
{
	srand(time(NULL));
	//stack<int> A;
	queue<int> A;
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;


	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);//start zegara
	for (int i(0); i <500000; i++) {
		A.push(rand());
	}
	//A = queue<int>();
	//A = stack<int>();
	QueryPerformanceCounter(&t2);
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << elapsedTime << " ms.\n";
	system("PAUSE");
    return 0;
}

